import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faStop,
  faPause,
  faPlay,
  faRunning,
} from '@fortawesome/free-solid-svg-icons';
class Stopwatch extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{paddingVertical: 20,justifyContent: 'center', alignItems: 'center'}}>
          <Text style={styles.timer}>00:00:00</Text>
        </View>
        <View
          style={{
            borderTopColor: '#424242',
            borderBottomColor: '#424242',
            borderTopWidth: 1,
            borderBottomWidth: 1,
            marginHorizontal: 20,
          }}
        />
        <View style={styles.lapBox}>
          <View style={{backgroundColor: '#f4f4f4', alignItems: 'center', paddingVertical: 15}}>
            <Text style={{fontSize: 18}}>1. 00:00:00</Text>
          </View>
          <View style={{backgroundColor: '#f4f4f4', alignItems: 'center'}}>
            <Text style={{fontSize: 18}}>1. 00:00:00</Text>
          </View>
        </View>
        <View style={styles.btnBox}>
          <TouchableOpacity style={styles.footBtn}>
            <FontAwesomeIcon size={25} icon={faStop} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.footBtn}>
            <FontAwesomeIcon size={25} icon={faPlay} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.footBtn}>
            <FontAwesomeIcon size={25} icon={faPause} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.footBtnLap}>
            <FontAwesomeIcon color={'white'} size={26} icon={faRunning} />
            <Text style={{justifyContent: 'center', alignItems: 'center'}}>
              lap
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  btnBox: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: '#12CEEE',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 70,
  },
  footBtn: {
    paddingHorizontal: 20,
  },
  footBtnLap: {
    paddingHorizontal: 20,
    width: 80,
    height: 80,
    backgroundColor: '#0d7a8f',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#000',
  },
  timer: {
    fontSize: 55,
    fontWeight: 'bold',
  },
  lapBox: {

  }
});

export default Stopwatch;
