import React, {PureComponent} from 'react';
import {View, Text, StyleSheet} from 'react-native';

import Time from '../../subcomponents/Time';
class TimeScreen extends PureComponent {
  state = {
    Hour: new Date().getHours(),
    Min: new Date().getHours(),
    second: new Date().getSeconds(),
  };
  getTime() {
    let time = new Date();
    let hour = time.getHours();
    let min = time.getMinutes();
    let sec = time.getSeconds();

    this.setState({
      Hour: hour,
      Min: min,
      second: sec,
    });
  }
  componentDidMount() {
    this.timer = setInterval(() => this.getTime(), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  render() {
    return (
      <View style={styles.contain}>
        <Text style={{color: 'lightskyblue', fontSize: 25}}>The time is</Text>
        <Time
            Hour={this.state.Hour}
            Min={this.state.Min}
            sec={this.state.second}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contain: {
    alignItems: 'center',
    flex: 1,
    padding: 20,
  },
});

export default TimeScreen;