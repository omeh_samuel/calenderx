import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Calender from '../../subcomponents/calender';
import Schedule from '../../subcomponents/shedule/schedule';
import Nav from '../../hoc/nav';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faCalendar,
  faClock,
  faHourglassHalf,
  faUserAlt,
} from '@fortawesome/free-solid-svg-icons';
class HomeScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: () => (
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text
            style={{marginLeft: 8, fontSize: 18, textTransform: 'capitalize'}}>
            omeh
          </Text>
          <FontAwesomeIcon style={{marginLeft: 8}} icon={faUserAlt} size={25} />
        </View>
      ),
      headerRight: () => (
        <View>
          <View style={styles.navi}>
            <TouchableOpacity>
              <FontAwesomeIcon
                size={25}
                style={styles.icon}
                icon={faCalendar}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Stopwatch')}>
              <FontAwesomeIcon
                size={25}
                style={styles.icon}
                icon={faHourglassHalf}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Time')}>
              <FontAwesomeIcon size={25} style={styles.icon} icon={faClock} />
            </TouchableOpacity>
          </View>
        </View>
      ),
    };
  };
  render() {
    return (
      <ScrollView style={{backgroundColor: '#F5F5F5'}}>
        <View
          style={{
            backgroundColor: '#ffffff',
            paddingVertical: 15,
            paddingHorizontal: 25,
          }}>
          <View style={{paddingBottom: 20}}>
            <Text style={{fontSize: 22, fontWeight: 'bold'}}>
              Your calender
            </Text>
          </View>
          <View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <Calender />
              <Calender />
              <Calender />
            </View>
            <TouchableOpacity style={styles.btn}>
              <Text
                style={{
                  textAlign: 'center',
                  textTransform: 'capitalize',
                  color: '#ffffff',
                  fontSize: 16,
                }}>
                open full calender
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <Schedule />
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  navi: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    margin: 10,
    color: '#424242',
  },
  btn: {
    backgroundColor: '#12CEEE',
    paddingVertical: 11,
    paddingHorizontal: 20,
    width: 180,
    borderRadius: 50,
    height: 45,
    marginVertical: 20,
  },
});
export default HomeScreen;
