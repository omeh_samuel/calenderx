import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faHourglassHalf,
  faClock,
  faCalendar,
} from '@fortawesome/free-solid-svg-icons';

const Nav = ({time}) => {
  return (
    <View style={styles.navi}>
      <TouchableOpacity onPress={() => time()}>
        <FontAwesomeIcon size={25} style={styles.icon} icon={faCalendar} />
      </TouchableOpacity>
      <TouchableOpacity>
        <FontAwesomeIcon size={25} style={styles.icon} icon={faHourglassHalf} />
      </TouchableOpacity>
      <TouchableOpacity>
        <FontAwesomeIcon size={25} style={styles.icon} icon={faClock} />
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  navi: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    margin: 10,
    color: '#424242',
  },
});
export default Nav;
