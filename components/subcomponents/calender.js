import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
const Calender = () => {
  return (
    <>
      <View style={styles.calender}>
        <Text style={styles.calenderText}>Fri</Text>
        <Text style={styles.calenderDay}>12</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  calender: {
    borderRadius: 25,
    backgroundColor: '#ffffff',
    width: 70,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 8,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    elevation: 5,
  },
  calenderText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#424242',
  },
  calenderDay: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#707070',
  },
});
export default Calender;
