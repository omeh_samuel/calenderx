import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Time = ({Hour, Min, sec}) => {
  return (
    <View
      style={{
        width: 200,
        height: 200,
        borderRadius: 250,
        backgroundColor: '#12CEEE',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Text
        style={{
          color: 'black',
          fontSize: 40,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {Hour + ':' + Min + ':' + sec}
      </Text>
    </View>
  );
};

export default Time;
