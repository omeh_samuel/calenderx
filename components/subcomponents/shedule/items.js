import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faTrash,
} from '@fortawesome/free-solid-svg-icons';

class Items extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    sub: false,
  };
  getSub = () => {
    this.setState({
      sub: true,
    });
    if (this.state.sub) {
      this.setState({
        sub: false,
      });
    }
  };

  render() {
    return (
      <TouchableOpacity style={styles.item} onPress={() => this.getSub()}>

        <View
          style={{
            paddingVertical: 20,
            paddingHorizontal: 20,
            alignItems: 'center',
            justifyContent: 'center',
            height: 60,
          }}>
          <View style={{flexDirection:'row',justifyContent: 'flex-start'}}>
           <View style={{width: 25,height: 25, backgroundColor:'#ffcccb',justifyContent: 'center',alignItems: 'center',borderRadius: 27}}>
             <FontAwesomeIcon onPress={this.props.remove}  size={16} icon={faTrash}/>
           </View>
            <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                  textTransform: 'capitalize',
                  marginLeft:10,
                }}>
              {this.props.task}
            </Text>
          </View>
        </View>
        {this.state.sub ? (
          <View style={styles.subComp}>
            <Text style={{fontSize: 18, fontWeight: 'bold'}}>
              {this.props.date}
            </Text>
            <Text style={{fontSize: 18, color: '#424242', marginLeft: 8}}>
              {this.props.time}
            </Text>
          </View>
        ) : null}
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  item: {
    // flexGrow: 0.5,
    borderRadius: 35,
    backgroundColor: '#fff',
    shadowOffset: {
      width: 2,
      height: 5,
    },
    shadowRadius: 5,
    shadowColor: '#000',
    elevation: 5,
    marginHorizontal: 10,
    flexDirection: 'row',
    marginVertical: 10,
    flexWrap: 'wrap',
  },
  subComp: {
    backgroundColor: '#12CEEE',
    flexGrow: 1,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    flexDirection: 'row',
  },
});
export default Items;
