import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Sched from './sched';
import Items from './items';
class Schedule extends Component {
  state = {
    taskInput: [
      {
        task: 'cook',
        id: 1,
        time: '12:01:pm',
        date: '12 Aug 2020',
      },
      {
        task: 'read thermodynamics',
        id: 2,
        time: '12:45:pm',
        date: '12 Mar 2020',
      },
      {
        task: 'buy food',
        id: 3,
        time: '06:01:pm',
        date: '2 Feb 2020',
      },
      {
        task: 'buy food for tibi while coming',
        id: 4,
        time: '11:01:pm',
        date: '12 Feb 2020',
      },
    ],
    currentTask: '',
    currentTime: '',
    currentDate: '',
  };
  AddSchedule() {
    let tArray = this.state.taskInput;
    if (
      this.state.currentDate &&
      this.state.currentTask &&
      this.state.currentTime !== ' '
    ) {
      tArray.unshift({
        task: this.state.currentTask,
        id: this.state.taskInput.length + 1,
        time: this.state.currentTime,
        date: this.state.currentDate,
      });
      console.log(this.state.currentDate);
      this.setState({
        taskInput: tArray,
      });
    }
  }
  removeTask(t) {
    let tasks = this.state.taskInput;
    let task = tasks.splice(t, 1);
    this.setState({
      taskInput: tasks,
    });
  }

  render() {
    return (
      <View style={{paddingHorizontal: 20, paddingVertical: 15}}>
        <Text style={{textTransform: 'capitalize', fontSize: 18}}>
          Schedule something
        </Text>
        <View>
          <Sched
            change={currentTask => this.setState({currentTask})}
            add={() => this.AddSchedule()}
            time={currentTime => this.setState({currentTime})}
            date={currentDate => this.setState({currentDate})}
          />
        </View>
        <Text
          style={{
            textTransform: 'capitalize',
            fontSize: 18,
            paddingVertical: 15,
            fontWeight: 'bold',
          }}>
          Tasks
        </Text>
        <View style={styles.itemsContent}>
          {this.state.taskInput.map((t, i) => {
            return (
              <Items
                remove={() => this.removeTask(t)}
                date={t.date}
                time={t.time}
                task={t.task}
                key={i}
              />
            );
          })}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  itemsContent: {
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
});
export default Schedule;
