import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Picker,
} from 'react-native';

const Sched = props => {
  return (
    <View>
      <TextInput
        style={styles.input}
        placeholder="Type here"
        onChangeText={currentTask => props.change(currentTask)}
        value={props.currentTask}
      />
      <View style={{flexDirection: 'row'}}>
        <TextInput
          style={{...styles.input, flex: 1.5, marginRight: 10}}
          placeholder="30"
          value={props.currentTime}
          onChangeText={currentTime => props.time(currentTime)}
        />

        <TextInput
          style={{...styles.input, flex: 1.5, marginRight: 10}}
          placeholder="date"
          value={props.currentDate}
          onChangeText={currentDate => props.date(currentDate)}
        />

        <TouchableOpacity style={styles.circleBtn} onPress={props.add}>
          <Text style={{fontSize: 45, color: '#000'}}>+</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  input: {
    borderRadius: 35,
    borderColor: '#424242',
    borderWidth: 3,
    paddingHorizontal: 15,
    paddingVertical: 8,
    fontSize: 16,
    marginVertical: 15,
  },
  circleBtn: {
    marginVertical: 15,
    width: 50,
    height: 50,
    borderRadius: 45,
    backgroundColor: '#12CEEE',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 5,
    shadowColor: '#000',
    elevation: 5,
  },
});
export default Sched;
