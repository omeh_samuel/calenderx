/**
 * Sample React Native App
 * https://github.com/faceboo k/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import TimeScreen from './components/pages/timer/timePage';
import Stopwatch from './components/pages/stopWatch/stopwatch';
import HomeScreen from './components/pages/home/home';
const Navigation = createStackNavigator(
  {
    Home: HomeScreen,
    Time: TimeScreen,
    Stopwatch: Stopwatch,
  },
  {
    initialRouteName: 'Home',
  },
);
const App = createAppContainer(Navigation);

export default App;
